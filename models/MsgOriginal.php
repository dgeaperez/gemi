<?php

/**
 * This is the model class for table "tbl_msgOriginal".
 *
 * The followings are the available columns in table 'tbl_msgOriginal':
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * The followings are the available model relations:
 * @property MsgTraducido[] $msgTraducidos
 */
class MsgOriginal extends CActiveRecord {
  const CAT_INTERFAZ = 'int';
  const CAT_MENU = 'mnu';
  const CAT_MISCELANEA = 'mis';
  const CAT_ATRIBUTO = 'atr';
  const CAT_INFORMACION = 'inf';
  const CAT_ERROR = 'err';
  const CAT_CORREO = 'mil';
  
  public $tabla = 'tbl_msgOriginal';

  /**
   * Returns the static model of the specified AR class.
   * @return MsgOriginal the static model class
   */
  public static function model($className=__CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    $this->verificarTabla();
    return $this->tabla;
  }

  public function verificarTabla() {
    /* Versión Original
     * 
     */
    $BaseDatos = Yii::app()->db->createCommand('SELECT DATABASE()')->queryScalar();

    $tbl_existe = Yii::app()->db->createCommand("SELECT COUNT( * ) AS Existe FROM information_schema.tables
WHERE table_schema = '" . $BaseDatos . "' AND table_name = '" . $this->tabla . "'")->queryScalar() > 0;
    if (!$tbl_existe) {
      $sql = <<<FIN
CREATE TABLE IF NOT EXISTS `$this->tabla` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `category` varchar(32) DEFAULT NULL,
 `message` text,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
FIN;
      Yii::app()->db->createCommand($sql)->execute();
    }
    /* Agregar aquí las subsecuentes modificaciones a esta/s tabla/s
     * y los procedimientos para actualizarlas
     */
  }
  
  public function titulo($i=0){
    return Yii::t('int_TIT_MSGORIGINAL', 'Mensaje Original|Mensajes Originales', $i);
  }

  public function primaryKey() {
    return 'id'; //No recuerdo xq lo dejé con uno solo
    // For composite primary key, return an array like the following
    // return array('pk1', 'pk2');
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
        array('category', 'length', 'max' => 32),
        array('message', 'safe'),
        // The following rule is used by search().
        // Please remove those attributes that should not be searched.
        array('id, category, message', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
        'msgTraducidos' => array(self::HAS_MANY, 'MsgTraducido', 'tbl_msgTraducido(id, language)'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'category' => Yii::t('atr_MSGO_CATEG', 'Categoría'),
        'message' => Yii::t('atr_MSGO_MENSAJE', 'Mensaje'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('category', $this->category, true);
    $criteria->compare('message', $this->message, true);

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }
  public function getCategoria(){
//  public function getCategoria($sCodigo){
    $aCategorias = self::getCategorias();
    return $aCategorias[substr($this->category,0,3)];
//    return $aCategorias[$sCodigo];
  }
  
  public function getCategorias() {
    return array(
        self::CAT_ATRIBUTO    => Yii::t('int_MSGO_ATRIBUTO', 'Atributo'),
        self::CAT_CORREO      => Yii::t('int_MSGO_CORREO', 'Correo'),
        self::CAT_ERROR       => Yii::t('int_MSGO_ERROR', 'Error'),
        self::CAT_INFORMACION => Yii::t('int_MSGO_INFORMACION', 'Información'),
        self::CAT_INTERFAZ    => Yii::t('int_MSGO_INTERFAZ', 'Interfaz'),
        self::CAT_MENU        => Yii::t('int_MSGO_MENU', 'Menú'),
        self::CAT_MISCELANEA  => Yii::t('int_MSGO_MISCELANEA', 'Miscelánea'),
    );
  }

}