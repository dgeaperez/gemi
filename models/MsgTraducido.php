<?php

/**
 * This is the model class for table "tbl_msgTraducido".
 *
 * The followings are the available columns in table 'tbl_msgTraducido':
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * The followings are the available model relations:
 * @property Idioma $language0
 * @property MsgOriginal $message
 */
class MsgTraducido extends CActiveRecord {

  public $tabla = 'tbl_msgTraducido';
  public $message;
  public $category;

  /**
   * Returns the static model of the specified AR class.
   * @return MsgTraducido the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    $this->verificarTabla();
    return $this->tabla;
  }

  public function verificarTabla() {
    /* Versión Original
     * 
     */
    $BaseDatos = Yii::app()->db->createCommand('SELECT DATABASE()')->queryScalar();

    $tbl_existe = Yii::app()->db->createCommand("SELECT COUNT( * ) AS Existe FROM information_schema.tables
WHERE table_schema = '" . $BaseDatos . "' AND table_name = '" . $this->tabla . "'")->queryScalar() > 0;
    if (!$tbl_existe) {
      $sql = <<<FIN
CREATE TABLE IF NOT EXISTS `$this->tabla` (
 `id` int(11) NOT NULL DEFAULT '0',
 `language` varchar(5) NOT NULL,
 `translation` text,
 PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
FIN;
      Yii::app()->db->createCommand($sql)->execute();
    }
    /* Agregar aquí las subsecuentes modificaciones a esta/s tabla/s
     * y los procedimientos para actualizarlas
     */
  }

  public function primaryKey() {
    return array('id', 'language'); //No recuerdo xq lo dejé con uno solo
    // For composite primary key, return an array like the following
    // return array('pk1', 'pk2');
  }

  public function titulo($i = 0) {
    return Yii::t('int_TIT_MSGT', 'Mensaje Traducido|Mensajes Traducidos', $i);
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
        //array('language', 'length', 'max'=>5),
        array('translation', 'safe'),
        // The following rule is used by search().
        // Please remove those attributes that should not be searched.
        array('message, category, language, translation', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
        'language0' => array(self::BELONGS_TO, 'Idioma', 'language'),
        'msgOrig' => array(self::BELONGS_TO, 'MsgOriginal', 'id'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
        'language' => Yii::t('atr_MSGT_LANGUAGE', 'Idioma'),
        'category' => Yii::t('atr_MSGT_CATEGORY', 'Categoría'),
        'translation' => Yii::t('atr_MSGT_TRANSLATION', 'Traducción'),
        'message' => Yii::t('atr_MSGT_MESSAGE', 'Mensaje'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('msgOrig.category', $this->category, true);
    $criteria->compare('msgOrig.message', $this->message, true);
    $criteria->with = 'msgOrig';
    //$criteria->addSearchCondition('msgOrig.message',$this->message);
    if (!Yii::app()->messages->forceTranslation)
      $criteria->addCondition("t.language <> '" . Yii::app()->sourceLanguage . "'");
    $criteria->compare('language', $this->language, true);
    $criteria->compare('translation', $this->translation, true);
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array('defaultOrder' => 'message',
                    'attributes' => array('message' => 'msgOrig.message',
                        '*')),
            ));
  }

}