<?php

/**
 * This is the model class for table "tbl_idioma".
 *
 * The followings are the available columns in table 'tbl_idioma':
 * @property string $idiCodigo
 * @property string $idiDescripcion
 * @property integer $idiActivo
 */
class Idioma extends CActiveRecord {

  const TODOS = 0;
  const ACTIVOS = 1;
  const INACTIVOS = 2;
  const VIGENTES = 3;
  const NO_VIGENTES = 4;
  const CONDICION_SQL = 5;

  public $tabla = 'tbl_idioma';

  /**
   * Returns the static model of the specified AR class.
   * @return Idioma the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    $this->verificarTabla();
    return $this->tabla;
  }

  public function verificarTabla($tabla = 'tbl_idioma') {
    /* Versión Original
     * 
     */
    $BaseDatos = Yii::app()->db->createCommand('SELECT DATABASE()')->queryScalar();

    $tbl_existe = Yii::app()->db->createCommand("SELECT COUNT( * ) AS Existe FROM information_schema.tables
WHERE table_schema = '" . $BaseDatos . "' AND table_name = '" . $this->tabla . "'")->queryScalar() > 0;
    if (!$tbl_existe) {
      $sql = <<<FIN
CREATE TABLE IF NOT EXISTS `$this->tabla` (
  `idiCodigo` varchar(5) NOT NULL COMMENT 'ISO 639 xx-xx',
  `idiDescripcion` varchar(45) DEFAULT 'SIN DESCRIPCION',
  `idiActivo` tinyint(1) DEFAULT '0',
  `idiBaja` datetime DEFAULT NULL,
  PRIMARY KEY (`idiCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
FIN;
      Yii::app()->db->createCommand($sql)->execute();
    }
    /* Agregar aquí las subsecuentes modificaciones a esta/s tabla/s
     * y los procedimientos para actualizarlas
     */
    //2013-01-04 Se agregan todos los registros de idioma de ISO 639-1
    $sql = "SELECT COUNT(*) FROM $this->tabla;";
    if (Yii::app()->db->createCommand($sql)->queryScalar() < 45) {
      $sql = "LOAD DATA LOCAL INFILE '" . Yii::getPathOfAlias('gemi') . DIRECTORY_SEPARATOR . "idioma.csv' "
              . "IGNORE "
              . "INTO TABLE $this->tabla "
              . "FIELDS TERMINATED BY ',' "
              . "LINES TERMINATED BY '\n' "
              . "(idiCodigo, idiDescripcion, idiActivo, idiBaja);";
      Yii::app()->db->createCommand($sql)->execute();
    }
    //Si la tabla no existía, se "ajustan" los registros de idioma según sourceLanguage e idiomaAux
    if (!$tbl_existe) {
      $sourceLanguage = Yii::app()->sourceLanguage;
      $idiomaAux = Yii::app()->getModule('gemi')->idiomaAux;
      $sql = <<<FIN
UPDATE `$this->tabla` SET `idiBaja`= NULL WHERE `idiCodigo` = '$idiomaAux';
FIN;
      Yii::app()->db->createCommand($sql)->execute();
      $sql = <<<FIN
UPDATE `$this->tabla` SET `idiActivo`=-1, `idiBaja`= NULL WHERE `idiCodigo` = '$sourceLanguage';
FIN;
      Yii::app()->db->createCommand($sql)->execute();
    }
  }

  public function titulo($i = 0) {
    return Yii::t('int_TIT_IDIOMA', 'Idioma|Idiomas', $i);
  }

  /**
   * @return value or array with the associated primary key
   */
  public function primaryKey() {
    return 'idiCodigo'; //No recuerdo xq lo dejé con uno solo
    // For composite primary key, return an array like the following
    // return array('pk1', 'pk2');
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
        array('idiActivo', 'numerical', 'integerOnly' => true),
        array('idiCodigo', 'length', 'max' => 5),
        array('idiDescripcion', 'length', 'max' => 20),
        // The following rule is used by search().
        // Please remove those attributes that should not be searched.
        array('idiCodigo, idiDescripcion, idiActivo', 'safe', 'on' => 'search'),
        array('idiCodigo, idiDescripcion', 'required', 'on' => 'insert, update'),
        array('idiCodigo', 'unique', 'caseSensitive' => false, 'on' => 'insert, update'),
        array('idiDescripcion', 'unique', 'caseSensitive' => false, 'on' => 'insert, update'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
        'idiCodigo' => Yii::t('atr_IDIO_IDICODIGO', 'Código'),
        'idiDescripcion' => Yii::t('atr_IDIO_IDIDESCRIP', 'Descripción'),
        'idiActivo' => Yii::t('atr_IDIO_IDIACTIVO', 'Activado'),
        'idiBaja' => Yii::t('atr_IDIO_IDIBAJA', 'Fecha de Baja'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('idiCodigo', $this->idiCodigo, true);

    $criteria->compare('idiDescripcion', $this->idiDescripcion, true);

    $criteria->compare('idiActivo', $this->idiActivo);

    return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
  }

  /**
   * @return bool idioma activo o no
   */
  public function getIdiomaActivo() {
    return $this->idiActivo ? Yii::t('int_GRAL_SI', 'Si') : Yii::t('int_GRAL_NO', 'No');
  }

  public function getIdiomaPorDefecto() {
    $lng = substr(Yii::app()->request->preferredLanguage, 0, strpos(Yii::app()->request->preferredLanguage, "_"));
    if ($this->IdiomaEstaDisponible($lng))
      return $lng;
    else if ($this->IdiomaEstaDisponible(Yii::app()->getModule('gemi')->idiomaAux))
      return Yii::app()->getModule('gemi')->idiomaAux;
    else
      return Yii::app()->sourceLanguage;
  }

  public function IdiomaEstaDisponible($lng) {
    if (strpos($lng, "_") !== false)
      $lng = substr($lng, 0, strpos($lng, "_"));
    return (count($this->findAll("idiCodigo = '{$lng}' and idiActivo = -1 and idiBaja IS NULL")) > 0);
  }

  public function getListarIdiomas($condicion = self::TODOS, $sSQL = null) {
    switch ($condicion) {
      case self::TODOS:
        $sSQL = '';
        break;
      case self::ACTIVOS:
        $sSQL = "idiActivo=-1 AND idiBaja IS NULL";
        break;
      case self::INACTIVOS:
        $sSQL = "idiActivo=0 AND idiBaja IS NULL";
        break;
      case self::VIGENTES:
        $sSQL = "idiBaja IS NULL";
        break;
      case self::NO_VIGENTES:
        $sSQL = "idiBaja IS NOT NULL";
        break;
      case self::CONDICION_SQL:
        //Experimental
        if ($sSQL === null)
          $sSQL = '';
        break;
    }
    try {
      $aRegistros = $this->findAll($sSQL);
    } catch (Exception $e) {
      $aRegistros = $this->findAll();
    }
    foreach ($aRegistros as $i => $aRegistro) {
      $aLista[$aRegistro->idiCodigo] = $aRegistro->idiDescripcion;
    }
    return $aLista;
  }

  public function getMostrarSelector() {
    $sHTML = <<<FIN
echo '<div  id="selector-idioma">';
CController::widget('application.modules.gemi.components.widgets.SelectorIdioma');
echo '</div>';
FIN;
    return $sHTML;
  }

}