<?php

class IdiomaController extends Controller {

  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = '//layouts/column2';
  /**
   * @var CActiveRecord the currently loaded data model instance.
   */
  private $_model;

  /**
   * @return array action filters
   */
  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules() {
    return array(
        array('allow', // allow all users to perform 'index' and 'view' actions
            'actions' => array('index', 'view'),
            'users' => array('*'),
        ),
        array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions' => array('create', 'update'),
            'users' => array('@'),
        ),
        array('allow', // allow admin user to perform 'admin' and 'delete' actions
            'actions' => array('admin', 'delete', 'undelete'),
            'users' => array('admin'),
        ),
        array('deny', // deny all users
            'users' => array('*'),
        ),
    );
  }

  /**
   * Displays a particular model.
   */
  public function actionView() {
    $this->render('view', array(
        'model' => $this->loadModel(),
    ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new Idioma;
    $model->setAttributes($model->getAttributes(), false);
    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model);

    if (isset($_POST['Idioma'])) {
      $model->attributes = $_POST['Idioma'];
      $model->idiCodigo = strtolower($model->idiCodigo);
      if ($model->save())
        $this->redirect(array('index'));
    }

    $this->render('create', array(
        'model' => $model,
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   */
  public function actionUpdate() {
    $model = $this->loadModel();

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model);

    if (isset($_POST['Idioma'])) {
      $model->attributes = $_POST['Idioma'];
      $model->idiCodigo = strtolower($model->idiCodigo);
      if ($model->save())
        $this->redirect(array('index'));
    }

    $this->render('update', array(
        'model' => $model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   */
  public function actionDeleteOriginal() {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel()->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    else
      throw new CHttpException(400, Yii::t('err_CCONTROLLER_400', 'Solicitud Incorrecta. Por favor no la repita.'));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   */
  public function actionDelete() {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $model = $this->loadModel();
      $model->idiBaja = new CDbExpression('NOW()');
      $model->save();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }
    else
      throw new CHttpException(400, Yii::t('err_CCONTROLLER_400', 'Solicitud Incorrecta. Por favor no la repita.'));
  }

  public function actionUndelete() {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $model = $this->loadModel();
      $model->idiBaja = new CDbExpression('NULL');
      $model->save();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }
    else
      throw new CHttpException(400, Yii::t('err_CCONTROLLER_400', 'Solicitud Incorrecta. Por favor no la repita.'));
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {/*
    $dataProvider = new CActiveDataProvider('Idioma');
    $this->render('index', array(
        'dataProvider' => $dataProvider,
    )); */
    $model = new Idioma('search');
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['Idioma']))
      $model->attributes = $_GET['Idioma'];

    $this->render('index', array(
        'model' => $model,
    ));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new Idioma('search');
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['Idioma']))
      $model->attributes = $_GET['Idioma'];

    $this->render('admin', array(
        'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   */
  public function loadModel() {
    if ($this->_model === null) {
      if (isset($_GET['id']))
        $this->_model = Idioma::model()->findbyPk($_GET['id']);
      if ($this->_model === null)
        throw new CHttpException(404, Yii::t('err_CCONTROLLER_404', 'La página solicitada no existe.'));
    }
    return $this->_model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'idioma-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

}
