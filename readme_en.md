#GeMI

##Gestor de Múltiples Idiomas (Multiple Language Manager)

----
###Presentation
This module provides:

 - Language Selector
 - CRUD or Languages (Idiomas model). NOTE: Delete operation is a soft delete.
 - Collects hardcoded strings written with Yii:t(), store them in a table allowing later translation.

ADDED: 2012-12-03
For non Spanish spoken people benefit; methods, properties and functions can be used in english language too. (*italic* form)

ADDED: 2013-01-02
There is no need to "force" to an inexistant language.
Yii provides a function to "force" the translation (See Installation)
The adventage of this change is enable a complete i18n implementation.
sourceLanguage parameter backs to its original use, and language parameter still is dynamic, idiomaAux (langAux) is a new parameter to set the "auxiliar" language to use before to set sourceLanguage to translate.
Example: 
Language XX is not defined or active, then GeMI ask for "idiomaAux" (langAux) and if it isn't active, sourceLanguage will be used.
language is dynamic, because is the language odf the browser, and it can be changed by the user if the language selector is showed.
sourceLanguage could be "es" and "idiomaAux" could be "en".
When the tables are created, the language's table will have 2 records not soft deleted: "sourceLanguage" activado, and "idiomaAux" desactivado.

####Important
Because the use of i18n lets use of any language, the database MUST have utf8 as character set and utf8_general_ci as collation. 
If the database isn't set in that way, the strings will have a wrong representation for languages like arabian, kanji, hebrew, etc...
You can run this SQL statament to solve that:
ALTER DATABASE mibbdd DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

----
###Installation
Copy'gemi' folder inside 'modules'. Or well, Create a folder, named 'gemi', inside protected/modules and donwload inside the source code from [repository](https://bitbucket.org/dgeaperez/gemi/).

Copy (or simlink ) the file BeginRequest.php in the folder components. If there an existant file, adapt it to include provided source code.

If you have installed EUpdateDialog, this step is not needed:
Copy (or simlink ) the folder EUpdateDialog from modules/extensions to protected/extensions. 
 
Edit config/main.php:

	//Forces source language never exists
	'sourceLanguage' => 'es',
	'language' => 'es',
 
 	'import'=>array(
  	    'application.models.*',
	    'application.modules.gemi.models.*',
 	 ),

 	// Asociates the behavior class with onBeginRequest event.
	// Set in the primary array affects to the whole application
	// In this case, changes the selected language of the application
	'behaviors' => array(
	    'onBeginRequest' => array(
	        'class' => 'application.components.BeginRequest'
	    ),
	),

	'modules'=>array(
	    ...
	    'gemi'=>array(
	        'mostrarTodos'=>true,     //showAll
	        'mostrarInactivos'=>true, //showDeactivated
          'idiomaAux' => 'en',
	    ), 
 
	'messages' => array(
	    'class' => 'CDbMessageSource',
	    'sourceMessageTable' => 'tbl_msgOriginal',
	    'translatedMessageTable' => 'tbl_msgTraducido',
	    'onMissingTranslation' => array('MensajeATraducir', 'load'),
      'forceTranslation' => true, //Lets or not to modify the text "hard coded"
	),

The tables are created "automagically".


----
####Configuration
#####mostrarTodos: (*showAll*)
> Lists all the languages defined or only the activated ones.  
> Possible values:  
>> + true,  
>> + false,  
>> + a logic operation to be evaluated, i.e.:

	"Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('UsuarioAvanzado')",

#####mostrarInactivos: (*showDeactivated*)
> Lists all the languages not soft deleted.  
> For instance, to test the transaltions before activate the language 
> Possible values: 
>> + true, 
>> + false, 
>> + a logic operation to be evaluated, i.e.:

	!Yii::app()->user->checkAccess('Admin')  && !Yii::app()->user->checkAccess('UsuarioAvanzado')

----
###Implementation
Add to the desidered menu this elements:

	array('label' => Yii::t('int','Idiomas'), 'url' => array('/gemi/idioma/index')),
	array('label' => Yii::t('int','Traducir'), 'url' => array('/gemi/msgTraducido/index')),

To use the Language Manager and the Message Translator, respectivelly.

All the strings to be translated MUST be

	Yii::t('categoría', 'mensaje'[, número])

To show the language selector, i.e.:

> In protected/views/layout/main.php

	<div  id="language-selector" style="float:right; margin:5px;">
	  <?php 
	    eval(Yii::app()->getModule('gemi')->mostrarSelector);
	  ?>
	</div>

----
###Notes:
* A Language is ACTIVE when the flag idiActivo is true
* A Language is DEACTIVE when the flag idiActivo is false
* A Language is AVAILABLE when the flag idiBaja is null
* A Language is UNAVAILABLE when the flag idiBaja is DATETIME
* At this moment, the lenguage identification is ISO 639 (2 chars)
* The field idiCodigo accepts 5 chars, as is indicated in ISO 3166-1 alfa-2 (es-ES, es-AR, etc) But will be considerated the 2 first chars.
* The language code MUST BE ALLWAYS in lowercase.
* If you want to allow only the users customization of the hardcoded strings, it is enough to not include the menu item gemi/idioma/index. At this moment this only aplies to spanish.

----
### Avaliable Functions
To use the funtions provided you can do it in two ways:

	$gemi = Yii::app()->getModule('gemi');
	$gemi->function;

Or:

	Yii::app()->getModule('gemi')->function;

#### mostrarTodos (*showAll*)
**Type:** Property  
**Value:** true or false 
**Explanation:** Evaluates the parameter configurated in main.php and returns *true/false*

#### mostrarInactivos (*showDeactivated*)
**Type:** Property  
**Value:** true or false 
**Explanation:** Evaluates the parameter configurated in main.php and returns *true/false*

#### mostrarSelector (*showSelector*)
**Type:** *magic getter*  
**Value:** texto  
**Explanation:** Returns a string to be evaluated and renders a mini form with the language selector. This will be affected for the porperty *mostrarTodos*  
**Example:**

	eval(Yii::app()->getModule('gemi')->mostrarSelector);

**Observations:**  
- It has its CSS style  
- To Do: The CSS style can be customized or to be integrated to the CSS system file.

#### listarIdiomaCompleto (*languageCompleteList*)
**Type:** *magic getter*  
**Value:** array  
**Explanation:** Returns an array with all languages defined in the table, no matter if is soft deleted or no activated  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasCompleto = $gemi->listarIdiomasCompleto;
	print_r($listaIdiomasCompleto);

	$listaIdiomaCompleto[array]:
		es => Español
		fr => François
		en => English

**Obervations:**  
- It's recomended native language for language name.  
- To Do: It should return an array with the follow structure

	$listaIdiomasCompleto[array]:
		es => Español, [active], [no soft deleted]
		fr => François, [active], [soft deleted]
		en => English, [no active], [no soft deleted]

#### listarIdiomasActivos (*activeLanguageList*)
**Type:** *magic getter*  
**Value:** array  
**Explanation:** Returns an array with all languages defined wich flag active is true  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasActivo = $gemi->listarIdiomasActivo;
	print_r($listaIdiomasActivo);

	$listaIdiomaActivo[array]:
		es => Español

**Obervations:**  
- It's recomended native language for language name.  

#### listarIdiomasInActivos (*deactiveLanguagesList*)
**Type:** *magic getter*  
**Value:** array  
**Explanation:** Returns an array with all languages defined wich flag active is false  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasInActivos = $gemi->listarIdiomasInActivos;
	print_r($listaIdiomasInActivos);

	$listaIdiomaActivo[array]:
		en => English

**Obervations:**  
- It's recomended native language for language name.  

#### listarIdiomasVigentes (*noSoftDeletedLanguagesList *)
**Type:** *magic getter*  
**Value:** array  
**Explanation:** Returns an array with all languages defined wich flag soft deleted is null  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasVigentes = $gemi->listarIdiomasVigentes;
	print_r($listaIdiomasVigentes);

	$listaIdiomaActivo[array]:
		es => Español
		en => English

**Obervations:**  
- It's recomended native language for language name.  
- To Do: It should return an array with the follow structure: 

	$listaIdiomasVigentes[array]:
		es => Español, [active]
		en => English, [no active]

#### listarIdiomasNoVigentes (*softDeletedLanguagesList*)
**Type:** *magic getter*  
**Value:** array  
**Explanation:** Returns an array with all languages defined wich flag soft deleted is *DATETIME*  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasNoVigente = $gemi->listarIdiomasNoVigente;
	print_r($listaIdiomasNoVigente);

	$listaIdiomaNoVigentes[array]:
		fr => François

**Obervations:**  
- It's recomended native language for language name.  
- To Do: It should return an array with the follow structure: 

	$listaIdiomasNoVigentes[array]:
		fr => François, [active] //fr was activated but it was soft deleted

#### ListarIdiomasPersonalizado($sSQL) (*CustomLanguagesList*)
**Type:** *Función*  
**Parámetro:** $sSQL
**Value:** array  
**Explanation:** Returns an array with all languages which criteria *where $sSQL*  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$ListarIdiomasPersonalizado = $gemi->ListarIdiomasPersonalizado('idiActivo = 0');
	print_r($ListarIdiomasPersonalizado);

	$ListarIdiomasPersonalizado[array]:
		en => English

**Obervations:**  
- It's recomended native language for language name.  
- To Do: It should return an array with the follow structure: 

	$ListarIdiomasPersonalizados[array]:
		en => English, [active], [no soft deleted]

#### idiomaPorDefecto (*DefaultLanguage*)
**Type:** *magic getter*  
**Value:** string  
**Explanation:** Returns a string with the default language  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$idiomaPorDefecto = $gemi->idiomaPorDefecto;
	print_r($idiomaPorDefecto);

	$idiomaPorDefecto[string]: es

**Obervations:**  
- To Do: It should be customized the default language in the table.

#### IdiomaEstaDisponible($sCodigoDeIdioma) (*LanguageIsAvailable*)
**Type:** *Function*  
**Parámetro:** $sLanguageCode
**Value:** True/False  
**Explanation:** Returns a *true/false* if the language is active or not, and it is not soft deleted  
**Example:**

	$gemi = Yii::app()->getModule('gemi');
	$IdiomaDisponible = $gemi->IdiomaEstaDisponible('es');
	$IdiomaNoDisponible = $gemi->IdiomaEstaDisponible('en');
	$IdiomaNoDisponible2 = $gemi->IdiomaEstaDisponible('es');

	print_r($IdiomaDisponible);
	$IdiomaDisponible[bool]: true

	print_r($IdiomaNoDisponible);
	$IdiomaDisponible[bool]: false

	print_r($IdiomaNoDisponible2);
	$IdiomaDisponible2[bool]: false

----
###Credits:
These resources were used to develop this module:

 - [LanguageSelector](http://www.yiiframework.com/wiki/293/manage-target-language-in-multilingual-applications-a-language-selector-widget-i18n)
 - [MissingTranslation](http://www.yiiframework.com/extension/db-missing-translations)
 - [EUpadteDialog](http://www.yiiframework.com/extension/eupdatedialog)
 - [Yii i18n](http://www.yiiframework.com/doc/guide/1.1/en/topics.i18n)
