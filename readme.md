#GeMI

##Gestor de Múltiples Idiomas

----
###Presentación
Se trata de un módulo que provee:

 - Selector de Idiomas
 - Crear, activar, desactivar, o dar de baja (borrado lógico) Idiomas
 - Recolectar todo texto escrito con la función Yii:t(), almacenarlo en una tabla y permitir su posterior traducción.

AGREGADO: 2012-12-03
Para beneficio de los no hispanoparlantes; los métodos, propiedades y funciones pueden utilizarse en inglés también. (escrito en *itálicas*)

AGREGADO: 2013-01-02
Se cambió la necesidad de "forzar" un idioma inexistente.
Yii provee ya una función que "fuerza" la traducción (Ver Instalacion).
La ventaja de este cambio es que permite una función i18n completa.
sourceLanguage vuelve a su función original,
language sigue siendo dinámico,
idiomaAux (langAux) es el idioma que "cumple" un rol universal y no es el "source".
Ejemplo: 
idioma XX no está definido o activo, entonces pregunta por "idiomaAux" (langAux) y si este no estuviera "activo", decanta por "sourceLanguage".
language es dinámico xq pregunto por el idioma del navegador, y cambia a voluntad del usuario si se muestra el selector.
sourceLanguage podría ser "es" y "idiomaAux" podría ser "en".
Cuando se "crean" las tablas, la de idioma tendrá 2 registros: uno para el idioma sourceLanguage activado, y el "idiomaAux" desactivado.

AGREGADO: 2013-03-05
No es necesario importar los modelos del módulo en la configuración principal.
Por lo tanto la línea "'application.modules.gemi.models.*'," en el arreglo "import" de main.php no es necesaria.

####Importante
Dado que el uso de i18n permite la utilización de cualquier idioma, es NECESARIO que la base de datos tenga su juego de caracteres com utf8 y el cotejamiento como utf8_general_ci.
Si no estuviera configurado de ese modo, se representarán los caracteres de manera errónea para idiomas con escrituras como la kanji, árabe, hebrea, etc...
Ejecutar esta instrucción en mysql resolvería el problema:
ALTER DATABASE mibbdd DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

----
###Instalación
Copiar la carpeta 'gemi' dentro de "modules". O bien, crear una carpeta 'gemi' dentro de protected/modules y  descargar del [repositorio](https://bitbucket.org/dgeaperez/gemi/) el código dentro de ella.

Copiar (o crear un enlace de) el archivo BeginRequest.php a la carpeta components. Si ya se tiene otro uso de BeginRequest, adaptarlo para que incluya el código provisto.

Si ya se tiene instalada EUpdateDialog, no hace falta realizar este paso: Copiar (o crear un enlace de) EUpdateDialog de modules/extensions a protected/extensions. 
 
Editar las siguientes secciones de config/main.php:

	'sourceLanguage' => 'es',
	'language' => 'es',
 
 	'import'=>array(
  	    'application.models.*',
 	 ),

 	// Asocia una clase behavior con el evento onBeginRequest.
	// Ponerlo en el arreglo primario, afecta a toda la aplicación
	// En este caso, lo q hace es cambiar el idioma elegido
	'behaviors' => array(
	    'onBeginRequest' => array(
	        'class' => 'application.components.BeginRequest'
	    ),
	),

	'modules'=>array(
	    ...
	    'gemi'=>array(
	        'mostrarTodos'=>true,
	        'mostrarInactivos'=>true,
          'idiomaAux' => 'en',
	    ), 
 
	'messages' => array(
	    'class' => 'CDbMessageSource',
	    'sourceMessageTable' => 'tbl_msgOriginal',
	    'translatedMessageTable' => 'tbl_msgTraducido',
	    'onMissingTranslation' => array('MensajeATraducir', 'load'),
      'forceTranslation' => true, //Indicará si se permite editar o no los textos en el "código"
	),

No es necesario crear tablas en la base de datos, se generan automáticamente.


----
####Configuración
#####mostrarTodos: (*showAll*)
> Listar todos los idiomas definidos o sólo los activos.  
> Puede usarse:  
>> + true,  
>> + false,  
>> + una operación lógica a evaluar, por ejemplo:

	"Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('UsuarioAvanzado')",

#####mostrarInactivos: (*showDeactivated*)
> Listar todos los idiomas no dados de baja.  
> Por ejemplo, para testear las traducciones antes de activar el idioma  
> puede usarse: 
>> + true, 
>> + false, 
>> + una operación lógica a evaluar:

	!Yii::app()->user->checkAccess('Admin')  && !Yii::app()->user->checkAccess('UsuarioAvanzado')

----
###Implementación
Agregar al menú deseado estos elementos:

	array('label' => 'Idiomas', 'url' => array('/gemi/idioma/index')),
	array('label' => 'Traducir', 'url' => array('/gemi/msgTraducido/index')),

Para acceder al Gestor de Idiomas y de Traducción de Mensajes, respectivamente.

Todas las cadenas de texto que deseen traducirse deben expresarse como 

	Yii::t('categoría', 'mensaje'[, número])

Para mostrar el selector de idiomas, por ejemplo:

> En protected/views/layout/main.php

	<div  id="language-selector" style="float:right; margin:5px;">
	  <?php 
	    eval(Yii::app()->getModule('gemi')->mostrarSelector);
	  ?>
	</div>

----
###Notas:
* Un idioma está ACTIVO cuando su flag idiActivo es verdadero
* Un idioma está INACTIVO cuando su flag idiActivo es falso
* Un idioma está VIGENTE cuando su campo idiBaja es nulo
* Un idioma está NO VIGENTE cuando su flag idiBaja es un dato DATETIME
* Al momento de esta versión se usa como identificador de idioma ISO 639 (2 letras)
* El campo idiCodigo acepta 5 caracteres tal como indica ISO 3166-1 alfa-2 (es-ES, es-AR, etc) Pero sólo se tomarán las 2 primeras.
* El código de idioma DEBE estar SIEMPRE en minúsculas.
* Si sólo se desea permitir que el usuario pueda cambiar los textos "fijos" de la aplicación, basta con no incluir en el menú gemi/idioma/index. De momento esto sólo aplica a Español.

----
### Funciones disponibles
Para acceder a las funciones provistas por el módulo de puede hacer de dos modos:

	$gemi = Yii::app()->getModule('gemi');
	$gemi->funcion;

O bien:

	Yii::app()->getModule('gemi')->funcion;

#####mostrarTodos: (*showAll*)
**Tipo:** Propiedad  
**Valor:** verdadero o falso   
**Explicación:** Evalúa el parámetro pasado en la configuración y retorna *true/false*

#### mostrarInactivos (*showDeactivated*)
**Tipo:** Propiedad  
**Valor:** verdadero o falso  
**Explicación:** Evalúa el parámetro pasado en la configuración y retorna *true/false*

#### mostrarSelector (*showSelector*)
**Tipo:** *magic getter*  
**Valor:** texto  
**Explicación:** Devuelve una cadena para ser evaluada y dibujará un mini formulario con un selector de idiomas. Éste se verá afectado por el valor de la propiedad *mostrarTodos*  
**Ejemplo:**

	eval(Yii::app()->getModule('gemi')->mostrarSelector);

**Observaciones:**  
- Tiene su propio estilo CSS  
- Próximamente: debería poder indicarse el estilo CSS o bien integrarlo a la hojas de estilo del sistema.

#### listarIdiomaCompleto (*languageCompleteList*)
**Tipo:** *magic getter*  
**Valor:** arreglo  
**Explicación:** Devuelve un arreglo con todos los idiomas definidos en la tabla, independientemente de su baja lógica o estado activo  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasCompleto = $gemi->listarIdiomasCompleto;
	print_r($listaIdiomasCompleto);

	$listaIdiomaCompleto[array]:
		es => Español
		fr => François
		en => English

**Observaciones:**  
- El nombre de los idiomas se sugiere que estén en el idioma original  
- Próximamente: debería entregar un arreglo con esta estructura

	$listaIdiomasCompleto[array]:
		es => Español, [activo], [sin baja lógica]
		fr => François, [activo], [con baja lógica]
		en => English, [inactivo], [sin baja lógica]

#### listarIdiomasActivos (*activeLanguageList*)
**Tipo:** *magic getter*  
**Valor:** arreglo  
**Explicación:** Devuelve un arreglo con todos los idiomas definidos en la tabla, cuyo estado es *activo*  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasActivo = $gemi->listarIdiomasActivo;
	print_r($listaIdiomasActivo);

	$listaIdiomaActivo[array]:
		es => Español

**Observaciones:**  
- El nombre de los idiomas se sugiere que estén en el idioma original  

#### listarIdiomasInActivos (*deactiveLanguagesList*)
**Tipo:** *magic getter*  
**Valor:** arreglo  
**Explicación:** Devuelve un arreglo con todos los idiomas definidos en la tabla, cuyo estado es *no activo*  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasInActivos = $gemi->listarIdiomasInActivos;
	print_r($listaIdiomasInActivos);

	$listaIdiomaActivo[array]:
		en => English

**Observaciones:**  
- El nombre de los idiomas se sugiere que estén en el idioma original  

#### listarIdiomasVigentes (*noSoftDeletedLanguagesList*)
**Tipo:** *magic getter*  
**Valor:** arreglo  
**Explicación:** Devuelve un arreglo con todos los idiomas definidos en la tabla, cuyo estado de baja es *nulo*  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasVigentes = $gemi->listarIdiomasVigentes;
	print_r($listaIdiomasVigentes);

	$listaIdiomaActivo[array]:
		es => Español
		en => English

**Observaciones:**  
- El nombre de los idiomas se sugiere que estén en el idioma original  
- Proximamente: quizá debería entregar un arreglo con esta estructura: 

	$listaIdiomasVigentes[array]:
		es => Español, [activo]
		en => English, [no activo]

#### listarIdiomasNoVigentes (*noSoftDeletedLanguagesList*)
**Tipo:** *magic getter*  
**Valor:** arreglo  
**Explicación:** Devuelve un arreglo con todos los idiomas definidos en la tabla, cuyo estado es *DATETIME*  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$listaIdiomasNoVigente = $gemi->listarIdiomasNoVigente;
	print_r($listaIdiomasNoVigente);

	$listaIdiomaNoVigentes[array]:
		fr => François

**Observaciones:**  
- El nombre de los idiomas se sugiere que estén en el idioma original  
- Proximamente: quizá debería entregar un arreglo con esta estructura: 

	$listaIdiomasNoVigentes[array]:
		fr => François, [activo] //fr estaba activo pero se dió de baja

#### ListarIdiomasPersonalizado($sSQL) (*CustomLanguagesList*)
**Tipo:** *Función*  
**Parámetro:** $sSQL
**Valor:** arreglo  
**Explicación:** Devuelve un arreglo con todos los idiomas que cumplan el criterio *where* que cumpla *$sSQL*  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$ListarIdiomasPersonalizado = $gemi->ListarIdiomasPersonalizado('idiActivo = 0');
	print_r($ListarIdiomasPersonalizado);

	$ListarIdiomasPersonalizado[array]:
		en => English

**Observaciones:**  
- El nombre de los idiomas se sugiere que estén en el idioma original  
- Proximamente: quizá debería entregar un arreglo con esta estructura: 

	$ListarIdiomasPersonalizados[array]:
		en => English, [activo], [sin baja lógica]

#### idiomaPorDefecto (*DefaultLanguage*)
**Tipo:** *magic getter*  
**Valor:** cadena  
**Explicación:** Devuelve una cadena con el idioma por defecto  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$idiomaPorDefecto = $gemi->idiomaPorDefecto;
	print_r($idiomaPorDefecto);

	$idiomaPorDefecto[string]: es

**Observaciones:**  
- Proximamente: quizá debería poder personalizarse el idioma por defecto a ingresar en la tabla.

#### IdiomaEstaDisponible($sCodigoDeIdioma) (*LanguageIsAvailable*)
**Tipo:** *Función*  
**Parámetro:** $sCodigoDeIdioma
**Valor:** Verdadero/Falso  
**Explicación:** Devuelve un valor *true/false* si el idioma está o no activo, y no está dado de baja  
**Ejemplo:**

	$gemi = Yii::app()->getModule('gemi');
	$IdiomaDisponible = $gemi->IdiomaEstaDisponible('es');
	$IdiomaNoDisponible = $gemi->IdiomaEstaDisponible('en');
	$IdiomaNoDisponible2 = $gemi->IdiomaEstaDisponible('es');

	print_r($IdiomaDisponible);
	$IdiomaDisponible[bool]: true

	print_r($IdiomaNoDisponible);
	$IdiomaDisponible[bool]: false

	print_r($IdiomaNoDisponible2);
	$IdiomaDisponible2[bool]: false

----
###Créditos:
Para el desarrollo de este módulo se utilizaron los siguientes recursos:

 - [LanguageSelector](http://www.yiiframework.com/wiki/293/manage-target-language-in-multilingual-applications-a-language-selector-widget-i18n)
 - [MissingTranslation](http://www.yiiframework.com/extension/db-missing-translations)
 - [EUpadteDialog](http://www.yiiframework.com/extension/eupdatedialog)

