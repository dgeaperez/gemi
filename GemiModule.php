<?php

/**
 * GeMI
 * ----
 * Gestión de Múltiples Idiomas
 * 
 * Créditos:
 * ---------
 * Para el desarrollo de este módulo se utilizaron los siguientes recursos:
 * - LanguageSelector (http://www.yiiframework.com/wiki/293/manage-target-language-in-multilingual-applications-a-language-selector-widget-i18n)
 * - MissingTranslation (http://www.yiiframework.com/extension/db-missing-translations)
 * 
 * Instalación:
 * ------------
 * - Copiar la carpeta 'gemi' dentro de "modules"
 * - Copiar el archivo BeginRequest.php a la carpeta components
 * - Copiar EUpdateDialog de modules/extensions a protected/extensions
 * - Editar las siguientes secciones de config/main.php:
 *  'sourceLanguage' => 'es',
 *  'language' => 'es',
 * 
 *    // Asocia una clase behavior con el evento onBeginRequest.
 *    // Ponerlo en el arreglo primario, afecta a toda la aplicación
 *    // En este caso, lo q hace es cambiar el idioma elegido
 *   'behaviors' => array(
 *        'onBeginRequest' => array(
 *            'class' => 'application.components.BeginRequest'
 *        ),
 *    ),
 *
 *  'modules'=>array(
 *    'gemi'=>array(
 *      //mostrarTodos: listar todos los idiomas definidos o sólo los activos
 *      //puede usarse: true, false, o una operación lógica a evaluar:
 *      //"Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('UsuarioAvanzado')",
 *      'mostrarTodos'=>true,
 *      //mostrarInactivos: listar todos los idiomas no dados de baja
 *      // Por ejemplo, para testear las traducciones antes de activar el idioma
 *      //puede usarse: true, false, o una operación lógica a evaluar:
 *      //!Yii::app()->user->checkAccess('Admin')  && !Yii::app()->user->checkAccess('UsuarioAvanzado')
 *      'mostrarInactivos'=>true,
 *      //Idioma alternativo a utilizar si el del navegador no está definido o activo.
 *      'idiomaAux' => 'en',
 *    ), 
 * 
 *    'messages' => array(
 *      'class' => 'CDbMessageSource',
 *      'sourceMessageTable' => 'tbl_msgOriginal',
 *      'translatedMessageTable' => 'tbl_msgTraducido',
 *      'onMissingTranslation' => array('MensajeATraducir', 'load'),
 *      'forceTranslation => true, 
 *    ),
 * 
 * Implementación:
 * - Agregar al menú deseado estos elementos:
 *   array('label' => 'Idiomas', 'url' => array('/gemi/idioma/index')),
 *   array('label' => 'Traducir', 'url' => array('/gemi/msgTraducido/index')),
 * - En la vista que se desea mostrar el selector de idiomas, por ejemplo,
 *   protectec/views/layout/main.php
 *     <div  id="language-selector" style="float:right; margin:5px;">
 *      <?php 
 *       eval(Yii::app()->getModule('gemi')->mostrarSelector);
 *      ?>
 *     </div>
 *
 * Notas:
 * * Un idioma está ACTIVO cuando su flag idiActivo es verdadero
 * * Un idioma está INACTIVO cuando su flag idiActivo es falso
 * * Un idioma está VIGENTE cuando su campo idiBaja es nulo
 * * Un idioma está NO VIGENTE cuando su flag idiBaja es un dato DATETIME
 * * Un idioma está ACTIVO cuando su flag idiActivo es verdadero
 * * Al momento de esta versión se usa como identificador de idioma ISO 639 (2 letras)
 * * El campo idiCodigo acepta 5 caracteres tal como indica ISO 3166-1 alfa-2 (es-ES, es-AR, etc)
 *
 * 
 * @package GeMI
 * @author DG Esteban A. Pérez <dgeaperez@gmail.com>
 * @version 1.0
 * @access public
 * 
 */
class GemiModule extends CWebModule {

  /**
   * Mostrar o no todos los idiomas
   * @property bool mostrarTodos true, false o una cadena que al evaluarse devuelve true o false 
   * Mostrar o no los idomas inactivos. Por ejemplo, para editar traducciones.
   * @property bool mostrarInactivos true, false o una cadena que al evaluarse devuelve true o false 
   */
  private $_mostrarTodos = true;
  private $_showAll = true;
  private $_mostrarInactivos = true;
  private $_showDeactivated = true;
  private $_idiomaAux = '';
  private $_langAux = '';
  private $_assetsUrl;

  public function init() {
    // this method is called when the module is being created
    // you may place code here to customize the module or the application
    // import the module-level models and components
    $this->setImport(array(
        'gemi.models.*',
        'gemi.components.*',
    ));

    Idioma::model()->verificarTabla();
    MsgOriginal::model()->verificarTabla();
    MsgTraducido::model()->verificarTabla();
  }

  /**
   * @return string La URL que contiene todos los archivos de personalización del módulo.
   */
  public function getAssetsUrl() {
    if ($this->_assetsUrl === null)
      $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('gemi.assets'));
    return $this->_assetsUrl;
  }

  /**
   * @param string La URL que contiene todos los archivos de personalización del módulo.
   */
  public function setAssetsUrl($value) {
    $this->_assetsUrl = $value;
  }


  public function beforeControllerAction($controller, $action) {
    if (parent::beforeControllerAction($controller, $action)) {
      // this method is called before any module controller action is performed
      // you may place customized code here
      return true;
    }
    else
      return false;
  }

  
  public function setMostrarTodos($mostrarTodos) {
    $this->_mostrarTodos = eval("return $mostrarTodos;");
    $this->_showAll = $this->_mostrarTodos;
  }

  public function setShowAll($showAll) {
    $this->setMostrarTodos($showAll);
  }
  
  public function getShowAll() {
    return $this->_showAll;
  }
  
  public function getMostrarTodos() {
    return $this->_mostrarTodos;
  }

  public function setMostrarInactivos($mostrarInactivos) {
    $this->_mostrarInactivos = eval("return $mostrarInactivos;");
    $this->_showDeactivated = eval("return $mostrarInactivos;");
  }

  public function setShowDeactivated($showDeactivated) {
    $this->setMostrarInactivos($showDeactivated);
  }
  
  public function getMostrarInactivos() {
    return $this->_mostrarInactivos;
  }

  public function getShowDeactivated() {
    return $this->_showDeactivated;
  }
  
  public function setIdiomaAux($idiomaAux) {
    $this->_idiomaAux = $idiomaAux;
    $this->_langAux = $this->_idiomaAux;
  }

  public function setLangAux($idiomaAux) {
    $this->setIdiomaAux($idiomaAux);
  }
  
  public function getIdiomaAux() {
    return $this->_idiomaAux;
  }
  
  public function getLangAux() {
    return $this->_langAux;
  }

  /**
   * 
   * @return string Devuelve una cadena que debe evaluarse para mostrar el selector de Idiomas
   */
  public function getMostrarSelector() {
    return Idioma::model()->mostrarSelector;
  }

  /**
   * 
   * @return string Returns a string to be evaluated to show the language's selector
   */
  public function getShowSelector() {
    return Idioma::model()->mostrarSelector;
  }

  /**
   * @return array Devuelve un arreglo con la lista COMPLETA de idiomas definidos 'es'=>'Español'
   */
  public function getListarIdiomasCompleto() {
    return Idioma::model()->listarIdiomas;
  }

  /**
   * @return array Returns an array with the COMPLETE list of defined languages 'es'=>'Español'
   */
  public function getLanguageCompleteList() {
    return Idioma::model()->listarIdiomas;
  }

  /**
   * 
   * @return array Devuelve un arreglo con el listado de idiomas activos 'es'=>'Español'
   */
  public function getListarIdiomasActivos() {
    return Idioma::model()->getListarIdiomas(Idioma::ACTIVOS);
  }

  /**
   * 
   * @return array Returns an array with the list of activated languages 'es'=>'Español'
   */
  public function getActiveLanguagesList() {
    return Idioma::model()->getListarIdiomas(Idioma::ACTIVOS);
  }

  /**
   * 
   * @return array Devuelve un arreglo con el listado de idiomas inactivos 'es'=>'Español'
   */
  public function getListarIdiomasInActivos() {
    return Idioma::model()->getListarIdiomas(Idioma::INACTIVOS);
  }

  /**
   * 
   * @return array Returns an array with the list of deactivated languages 'es'=>'Español'
   */
  public function getDeactiveLanguagesList() {
    return Idioma::model()->getListarIdiomas(Idioma::INACTIVOS);
  }

  /**
   * 
   * @return array Devuelve un arreglo con el listado de idiomas vigentes 'es'=>'Español'
   */
  public function getListarIdiomasVigentes() {
    return Idioma::model()->getListarIdiomas(Idioma::VIGENTES);
  }

  /**
   * 
   * @return array Returns an array with the list of no soft deleted languages 'es'=>'Español'
   */
  public function getNoSoftDeletedLaguagesList() {
    return Idioma::model()->getListarIdiomas(Idioma::VIGENTES);
  }

  /**
   * 
   * @return array Devuelve un arreglo con el listado de idiomas no vigentes 'es'=>'Español'
   */
  public function getListarIdiomasNoVigentes() {
    return Idioma::model()->getListarIdiomas(Idioma::NO_VIGENTES);
  }

  /**
   * 
   * @return array Returns an array with the list of disabled languages 'es'=>'Español'
   */
  public function getSoftDeletedLaguagesList() {
    return Idioma::model()->getListarIdiomas(Idioma::NO_VIGENTES);
  }

  /**
   * @param string $sSQL cadena se adicionará al "WHERE". 
   * Si tuviera un error o no se indica devolverá lo mismo que ListarIdiomasCompleto
   * @return array Devuelve un arreglo con el listado personalizado de idiomas 'es'=>'Español'
   * @example $aIdiomasExcepto_es=$gemi->ListarIdiomasPersonalizado("idiCodigo != 'es' and idiBaja IS NULL");
   */
  public function ListarIdiomasPersonalizado($sSQL) {
    return Idioma::model()->getListarIdiomas(Idioma::CONDICION_SQL, $sSQL);
  }

  /**
   * @param string $sSQL String added to "WHERE". 
   * If it has an error or is no set, will return the same result as CompleteLanguagesList
   * @return array Returns an array with custom languages list 'es'=>'Español'
   * @example $aLanguagesExcept_es=$gemi->CustomLanguageList("idiCodigo != 'es' and idiBaja IS NULL");
   */
  public function CustomLanguagesList($sSQL) {
    return Idioma::model()->getListarIdiomas(Idioma::CONDICION_SQL, $sSQL);
  }

  /**
   * @return string Devuelve el idioma por defecto a utilizar.
   * Se espera que indique idioma del navegador si está definido, activo y no dado de baja.
   * Caso contrario, devolverá el idioma declarado en sourceLanguage de Yii. 
   * Si se siguieron las instrucciones de GeMI, devolverá '!!'.
   */
  public function getIdiomaPorDefecto() {
    return Idioma::model()->idiomaPorDefecto;
  }

  /**
   * @return string Returns the default language.
   * If the browser language is defined, it will be returned, if it is defined and active.
   * Else, it will return the value of sourceLanguage. 
   * If you follow the GeMI instructions, this value must be '¡¡'.
   */
  public function getDefaultLanguage() {
    return Idioma::model()->idiomaPorDefecto;
  }

  /**
   * 
   * @param  string $lng El idioma a saber si está disponible o no.
   * @return bool true o false si el idioma está disponible o no. 
   * null si no se $lang no se indica.
   */
  public function IdiomaEstaDisponible($lng = null) {
    if ($lng === null)
      return null;
    return Idioma::model()->IdiomaEstaDisponible($lng);
  }

  /**
   * 
   * @param  string $lng Language code to know if it is available or not.
   * @return bool true o false depending the langauge availability. 
   * null if $lang is not set.
   */
  public function LanguageIsAvailable($lng = null) {
    if ($lng === null)
      return null;
    return Idioma::model()->IdiomaEstaDisponible($lng);
  }

}
