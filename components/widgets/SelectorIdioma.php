<?php
/**
 * Description of LanguageSelector
 *
 * @author esteban
 */
class SelectorIdioma extends CWidget
{
    public function run()
    {
        $idiomaActual = Yii::app()->language;
        $gemi = Yii::app()->getModule('gemi');
        //$mostrar=eval("return $gemi->mostrarTodos;");
        $idiomas = $gemi->mostrarTodos ?
                     $gemi->listarIdiomasVigentes:
                     $gemi->listarIdiomasActivos;
        if (is_array($idiomas) && count($idiomas) > 1)
          $this->render('selectorIdioma', array('idiomaActual' => $idiomaActual, 'idiomas'=>$idiomas));
    }
}
?>
