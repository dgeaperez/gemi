<?php

/**
 * Maneja el evento onMissingTranslation
 */
class MensajeATraducir extends CApplicationComponent {

  /**
   * Agrega los textos explícitos del código para ser traducidos o modificados
   * por el usuario.
   * La lógica es la siguiente:
   * - Se indica a Yii que el idioma original es "!!" que no existe
   * - Conforme va ejecutando Yii::t(), verifica si existe esa cadena
   * - Si no existe, la almacena y luego se podrá editar vía "Traducir Mensajes"
   */
  public static function load($event) {
    // Carga los mensajes		
    $original = MsgOriginal::model()->find('message=:mensaje AND category=:categoria', array(':mensaje' => $event->message, ':categoria' => $event->category));

    // Si no encuentra el mensaje, existe su categoría (que ahora es única)
    if (!$original){
      $original = MsgOriginal::model()->find('category=:categoria', array(':categoria' => $event->category));
      if (($original instanceof MsgOriginal) &&
           $original->message != $event->message){
        //El mensaje cambió en el código, y hay que actualizarlo.
        $original->message = $event->message;
        $original->setScenario('update');
        $original->save();
      }
    }
    // Si aún no se encuentra el mensaje
    if (!$original) {
      // Lo agregamos
      $original = new MsgOriginal;

      $original->category = $event->category;
      $original->message = $event->message;
      $original->save();

      $lastID = Yii::app()->db->lastInsertID;
    }

    if ($event->language != Yii::app()->sourceLanguage
            || $event->sender->forceTranslation) {
      // Hacemos lo mismo con las traducciones	
      $traduccion = MsgTraducido::model()->find('language=:idioma AND id=:id', 
                                               array(':idioma' => $event->language, 
                                                     ':id' => $original->id)
                                               );

      // Si no la encontramos, la guardamos
      if (!$traduccion) {
        $original = MsgOriginal::model()->find('category=:categoria', array(':categoria' => $event->category));

        // Agregando...
        $traduccion = new MsgTraducido;

        $traduccion->id = $original->id;
        $traduccion->language = $event->language;
        $traduccion->translation = $event->message;
        $traduccion->save();
      }
    }
  }

}
