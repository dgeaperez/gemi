<?php
$this->breadcrumbs=array(
	$model->titulo()=>array('index'),
	$model->idiCodigo=>array('view','id'=>$model->idiCodigo),
	Yii::t('int_GRL_MODIFICAR', 'Modificar'),
);

$this->menu=array(
	array('label'=>Yii::t('int_GRL_LISTAR', 'Listar'), 'url'=>array('index')),
	array('label'=>Yii::t('int_GRL_CREAR', 'Crear'), 'url'=>array('create')),
	array('label'=>Yii::t('int_GRL_VER','Ver'), 'url'=>array('view', 'id'=>$model->idiCodigo)),
	//array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('int_GRL_MODIFICAR', 'Modificar').' '.$model->idiCodigo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>