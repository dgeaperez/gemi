<?php
$this->breadcrumbs=array(
  $model->titulo()=>array('index'),
	$model->idiCodigo,
);

$this->menu=array(
	array('label'=>Yii::t('int_GRL_LISTAR', 'Listar'), 'url'=>array('index')),
	array('label'=>Yii::t('int_GRL_CREAR', 'Crear'), 'url'=>array('create')),
	array('label'=>Yii::t('int_GRL_MODIFICAR', 'Modificar'), 'url'=>array('update', 'id'=>$model->idiCodigo)),
	array('label'=>Yii::t('int_GRL_ELIMINAR', 'Eliminar'), 'url'=>'#', 'visible'=>$model->idiCodigo!=$this->_sl && $model->idiBaja == null, 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idiCodigo),'confirm'=>Yii::t('int_GRL_ELIMINAR_PREG', '¿Está seguro que desea eliminar este registro?'))),
	array('label'=>Yii::t('int_GRL_RECUPERAR', 'Recuperar'), 'url'=>'#', 'visible'=>$model->idiCodigo!=$this->_sl && $model->idiBaja != null, 'linkOptions'=>array('submit'=>array('undelete','id'=>$model->idiCodigo),'confirm'=>Yii::t('int_GRL_RECUPERAR_PREG', '¿Está seguro que desea recuperar este registro?'))),
	//array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('int_GRL_VER','Ver').' '.$model->idiDescripcion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idiCodigo',
		'idiDescripcion',
		array(
            'name'=>'idiActivo',
            'value'=>$model->getIdiomaActivo()
        ),
    'idiBaja',
	),
)); ?>
