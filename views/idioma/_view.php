<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idiCodigo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idiCodigo), array('view', 'id'=>$data->idiCodigo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idiDescripcion')); ?>:</b>
	<?php echo CHtml::encode($data->idiDescripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idiActivo')); ?>:</b>
	<?php echo CHtml::encode($data->getIdiomaActivo()); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idiBaja')); ?>:</b>
	<?php echo CHtml::encode($data->idiBaja); ?>
	<br />


</div>