<?php
$this->breadcrumbs = array(
    $model->titulo(),
);

$this->menu = array(
    array('label' => Yii::t('int_GRL_CREAR', 'Crear'), 'url' => array('create')),
        //array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php echo $model->titulo(); ?> </h1>

<?php
$sRutaTema =  Yii::app()->controller->module->assetsUrl.DIRECTORY_SEPARATOR.'gridview' . DIRECTORY_SEPARATOR;
$cmpCodigo = 'idiCodigo';

$botones = array('class' => 'CButtonColumn',
    'template' => '{ver}{editar}{borrar}{recuperar}',
    'buttons' => array
        (
        'ver' => array
            (
            'label' => Yii::t('int_GRL_VER','Ver'),
            'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data["' . $cmpCodigo . '"]))',
            'visible' => '$data->idiCodigo != Yii::app()->sourceLanguage && $data->idiBaja == null',
            'imageUrl' => $sRutaTema . "view.png",
        ),
        'editar' => array
            (
            'label' => Yii::t('int_GRL_MODIFICAR', 'Modificar'),
            'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->' . $cmpCodigo . '))',
            'visible' => '$data->idiCodigo != Yii::app()->sourceLanguage && $data->idiBaja == null',
            'imageUrl' => $sRutaTema . "update.png",
        ),
        'borrar' => array
            (
            'label' => Yii::t('int_GRL_ELIMINAR', 'Eliminar'),
            'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->' . $cmpCodigo . '))',
            //'url' => '#',
            'imageUrl' => $sRutaTema . 'delete.png',
            'visible' => '$data->idiCodigo != Yii::app()->sourceLanguage && $data->idiBaja == null',
            'click' => 'function(data) 
              {        
                if(!confirm(' . CJavaScript::encode(Yii::t('int_GRL_ELIMINAR_PREG', '¿Está seguro que desea eliminar este registro?')) . ')) return false;
                    $.ajax(
                    {
                     type: "POST",
                     url: $(this).attr("href"),
                     success: function() { $("#idioma-grid").yiiGridView.update("idioma-grid"); },
                     error: function() { alert("' . Yii::t('int_ERR_OPERACION_FALLO', 'La operación falló.') . '");}
                    });
              return false;
              }',
        ),
        'recuperar' => array
            (
            'label' => Yii::t('int_GRL_RECUPERAR', 'Recuperar'),
            'url' => 'Yii::app()->controller->createUrl("undelete",array("id"=>$data->' . $cmpCodigo . '))',
            'imageUrl' => $sRutaTema . 'undelete.png',
            'visible' => '$data->idiCodigo != "es" && $data->idiBaja != null',
            'click' => 'function(data) 
              {        
                if(!confirm(' . CJavaScript::encode(Yii::t('int_GRL_RECUPERAR_PREG', '¿Está seguro que desea recuperar este registro?')) . ')) return false;
                    $.ajax(
                    {
                     type: "POST",
                     url: $(this).attr("href"),
                     success: function() { $("#idioma-grid").yiiGridView.update("idioma-grid"); },
                     error: function() { alert("' . Yii::t('int_ERR_OPERACION_FALLO', 'La operación falló.') . '");}
                    });
               return false;
              }',
        ),
    ),
);

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'idioma-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $sRutaTema . 'styles.css',
    'rowCssClassExpression' => '($row%2)?"even".($data->idiBaja!=null?"_del":""):"odd".($data->idiBaja!=null?"_del":"")',
    'filter' => $model,
    'columns' => array(
        'idiCodigo',
        'idiDescripcion',
        //'idiActivo',
        array(
            'name' => 'idiActivo',
            'value' => '$data->IdiomaActivo',
            'filter' => array(-1 => Yii::t('int_GRAL_SI', 'Si'), 0 => Yii::t('int_GRAL_NO', 'No')),
        ),
        $botones,
    ),
));
?>

