<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'idioma-form',
	'enableAjaxValidation'=>true,
)); 
  $esUnAlta = $model->isNewRecord;
?>

   <p class="note"><span class="required"><?php echo Yii::t('inf_GRL_CAMPO_OBLIGATORIO', 'Los campos con * son obligatorios.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

  <div class="row">
    <?php echo $form->labelEx($model,'idiCodigo'); ?>
    <?php echo $form->textField($model,'idiCodigo',array('size'=>2,'maxlength'=>2)); ?>
    <?php echo $form->error($model,'idiCodigo'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'idiDescripcion'); ?>
    <?php echo $form->textField($model,'idiDescripcion',array('size'=>20,'maxlength'=>20)); ?>
    <?php echo $form->error($model,'idiDescripcion'); ?>
  </div>

	<div class="row">
		<?php echo $form->labelEx($model,'idiActivo'); ?>
		<?php echo $form->checkBox($model,'idiActivo',array('value' => -1, 'uncheckedValue' => 0)); ?>
		<?php echo $form->error($model,'idiActivo'); ?>
	</div>

	<div class="row buttons">
    <?php echo CHtml::submitButton($esUnAlta ? Yii::t('int_GRL_CREAR', 'Crear') : Yii::t('int_GRL_GUARDAR','Guardar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->