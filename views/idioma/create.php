<?php
$this->breadcrumbs=array(
  $model->titulo()=>array('index'),
  Yii::t('int_GRL_CREAR', 'Crear'),
);

$this->menu=array(
  array('label'=>Yii::t('int_GRL_LISTAR', 'Listar'), 'url'=>array('index')),
  //array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php Yii::t('int_GRL_CREAR', 'Crear');?> </h1>

<p class="note"><?php echo Yii::t('int_IDIOMA_LYND', 
        'Los códigos de idioma se representan según {ISO6391} y es ampliabe a {i18n}.',
        array('{ISO6391}'=>CHtml::link('ISO 6391','http://en.wikipedia.org/wiki/ISO_639-1_language_matrix', array('target'=>'_blank')),
              '{i18n}' =>CHtml::link('locale','http://en.wikipedia.org/wiki/Locale',array('target'=>'_blank'))));?></p>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>