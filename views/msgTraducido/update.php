<?php
$this->breadcrumbs=array(
	$model->titulo()=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	Yii::t('int_MSGT_TRADUCIR', 'Traducir'),
);

$this->menu=array(
	//array('label'=>Yii::t('int_GRL_LISTAR', 'Listar'), 'url'=>array('index')),
	//array('label'=>Yii::t('int_GRL_CREAR', 'Crear'), 'url'=>array('create')),
	//array('label'=>Yii::t('int_GRL_VER', 'Ver'), 'url'=>array('view', 'id'=>$model->id)),
	//array('label'=>Yii::t('int_GRL_ELIMINAR', 'Eliminar'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('int_GRL_ELIMINAR_PREG', '¿Está seguro que desea eliminar este registro?'))),
	array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php echo $model->titulo().": <em>".$model->message->message."</em>"; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'txtOriginal'=>$txtOriginal)); ?>