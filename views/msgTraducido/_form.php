<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'msg-traducido-form',
	'enableAjaxValidation'=>false,
)); ?>
  <p class="note"><?php echo Yii::t('inf_MSGT_LYND', 
          'NOTA: Si observa leyendas encerradas entre llaves {} o con símbolos tales como % o similar, DEBEN conservarse pues indican lugares que serán reemplazados por valores dinámicos.<br />'.
          'Si encuentra varios valores separados por un signo &#166; significa que deberá traducir las formas respectivas de plural según este {enlace}. [En inglés]', 
          array('{enlace}' =>CHtml::link(Yii::t('int_GRL_ENLACE', 'enlace'), 'http://unicode.org/repos/cldr-tmp/trunk/diff/supplemental/language_plural_rules.html', array('target'=>'_blank'))));?></p>

  <p class="note"><span class="required"><?php echo Yii::t('inf_GRL_CAMPO_OBLIGATORIO', 'Los campos con * son obligatorios.'); ?></p>

  <div class="row">
    <p class="hint"><b><?php echo Yii::t('int_GRL_TXT_ORIGINAL', 'Texto Original'); ?></b></p>
    <p class="hint"><?php echo $txtOriginal; ?></p>
  </div>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,$model->language0->idiDescripcion.' <span class="required">*</span>'); ?>
		<?php echo $form->textArea($model,'translation',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'translation'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('int_GRL_CREAR', 'Crear') : Yii::t('int_GRL_GUARDAR','Guardar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->