<?php
$this->breadcrumbs = array(
    $model->titulo() => array('index'),
    Yii::t('int_GRL_ADMINISTRAR', 'Administrar'),
);

$this->menu = array(
    array('label' => Yii::t('int_GRL_LISTAR', 'Listar'), 'url' => array('index')),
        //array('label'=>Yii::t('int_GRL_CREAR', 'Crear'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('msg-traducido-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('int_GRL_ADMINISTRAR', 'Administrar'); ?> </h1>

<p><?php echo Yii::t('inf_AYD_COMPARADOR','Opcionalmente puede utilizar un operador de comparación '.
                     'al comienzo de cada valor de la búsqueda para indicar cómo debe realizarse.<br />'.
                     'Operadores: (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>, <b>=</b>)'); ?></p>

<?php echo CHtml::link(Yii::t('int_GRL_BUSQ_AVANZADA','Búsqueda Avanzada'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
  <?php
  $this->renderPartial('_search', array(
      'model' => $model,
      'gemi'  => $gemi,
  ));
  ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'msg-traducido-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'message',
            'value' => '$data->msgOrig->message',
        ),
        array(
            'name' => 'category',
            'value' => '$data->msgOrig->categoria',
            'filter' => MsgOriginal::model()->getCategorias(),
        ),
        array(
            'name' => 'language',
            'value' => '$data->language0->idiDescripcion',
            'filter' => Yii::app()->messages->forceTranslation ? 
                        $gemi->listarIdiomasVigentes :
                        $gemi->ListarIdiomasPersonalizado("idiBaja IS NULL AND idiCodigo <> '".Yii::app()->sourceLanguage."'"),
        ),
        'translation',
        array(
            'class' => 'CButtonColumn',
            'buttons' => array(
                'update' => array(
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id, "lng"=>$data->language))',
                    'click' => 'updateDialogUpdate',
                    'visible'=>'($data->language0->idiCodigo!=Yii::app()->sourceLanguage) || Yii::app()->messages->forceTranslation',
                ),
            ),
            'template' => '{update}',
        ),
    ),
));
?>
<?php
$this->widget('ext.EUpdateDialog.EUpdateDialog', array(
   // 'height' => 200,
   // 'resizable' => true,
    'width' => 520,
    'title' =>Yii::t('int_MSGT_VENTANA_TRADUCIR', 'Traducir'),
));
?>
