<?php
$this->breadcrumbs=array(
	$model->titulo()=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('int_GRL_LISTAR', 'Listar'), 'url'=>array('index')),
	//array('label'=>Yii::t('int_GRL_CREAR', 'Crear'), 'url'=>array('create')),
	array('label'=>Yii::t('int_MSGT_TRADUCIR', 'Traducir'), 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>Yii::t('int_GRL_ELIMINAR', 'Eliminar'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('int_GRL_ELIMINAR_PREG', '¿Está seguro que desea eliminar este registro?'))),
	array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php echo $model->titulo().' #'.$model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'language',
		'translation',
	),
)); ?>
