<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'category'); ?>
		<?php echo $form->dropDownList($model,'category',array(""=>Yii::t('int_GRL_TODAS', 'Todas'))+MsgOriginal::model()->getCategorias()); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'language'); ?>
		<?php echo $form->dropDownList($model,'language',array(""=>Yii::t('int_GRL_TODOS', 'Todos'))+(
                        Yii::app()->messages->forceTranslation ? 
                        $gemi->listarIdiomasVigentes :
                        $gemi->ListarIdiomasPersonalizado("idiBaja IS NULL AND idiCodigo <> '".Yii::app()->sourceLanguage."'"))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'translation'); ?>
		<?php echo $form->textArea($model,'translation',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('int_GRL_BUSCAR', 'Buscar')); ?>
		<?php echo CHtml::resetButton(Yii::t('int_GRL_LIMPIAR', 'Limpiar'), array('id'=>'form-reset-button')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->